# -*- coding: utf-8 -*-

{
    'name': 'sigu_cart',
    'summary': """
    sigu_cart
    """,
    'description': 'sigu_cart',
    'author': 'Douglas',
    'version': '1.0.0',
    'depends': ['base'],
    'data': [
        'views/carnet_sigu_view.xml',
        'security/ir.model.access.csv',
        'views/menuitem0.xml',
        'views/menuitem1.xml',
        'views/menuitem2.xml',
        #'views/menuitem3.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}