# -*- coding: utf-8 -*-

from odoo import models, fields

class SiguNucleoForPrograma(models.Model):
    _name = 'sigu.nucleo.for.programa'
    
    nucleo_for_program = fields.Integer(string='Código de Nucleo para Programa')
    codigo_programa = fields.Char(string='Código Programa', required=True)
    name_programa = fields.Char(string='Nombre Programa')
    codigo_nucleo = fields.Char(string='Código Núcleo', required=True)
    name_nucleo = fields.Char(string='Nombre Núcleo')