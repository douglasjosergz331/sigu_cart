# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SiguEstudiante(models.Model):
    _name = 'sigu.estudiante'
    
    codigo_student = fields.Integer(string='Cédula', required=True)
    first_name = fields.Char(string='First Name')
    second_first_name = fields.Char(string='Second Name')
    first_surname = fields.Char(string='First Surname')
    second_surname = fields.Char(string='Second Surname')
    location = fields.Char(string='Location')
    nucleo_for_program = fields.Many2one('nucleo_for_program', string='nucleo para programas codigo')

    @api.depends('codigo_student')
    def _compute_estudiante_count(self):
        for record in self:
            record.estudiante_count = len(record)