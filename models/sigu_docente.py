# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SiguDocente(models.Model):
    _name = 'sigu.docente'
    
    codigo_teacher = fields.Integer(string='Cédula', required=True)
    first_name = fields.Char(string='First Name')
    second_first_name = fields.Char(string='Second Name')
    first_surname = fields.Char(string='First Surname')
    second_surname = fields.Char(string='Second Surname')
    location = fields.Char(string='Location')
    profession_name = fields.Char(string='First Name Profession')
    nucleo_for_program = fields.Many2one('nucleo_for_program', string='nucleo para programas codigo')

    @api.depends('codigo_teacher')
    def _compute_docente_count(self):
        for record in self:
            record.docente_count = len(record)